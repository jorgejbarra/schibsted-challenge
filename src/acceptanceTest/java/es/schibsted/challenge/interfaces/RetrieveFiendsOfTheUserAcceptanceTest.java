package es.schibsted.challenge.interfaces;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.OK;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class RetrieveFiendsOfTheUserAcceptanceTest {

    @Autowired
    @SuppressWarnings("unused")
    private TestRestTemplate restTemplate;

    private UserPublicDiver userDiver;

    @Before
    public void setUp() {
        userDiver = new UserPublicDiver(restTemplate);
    }

    @Test
    public void aNewUserShouldNotHaveAnyFriends() {

        //given
        final String userName = "israel";
        userDiver.newUserWithDefaultPass(userName);

        //when
        final ResponseEntity<UserFriendsResource> response = restTemplate.withBasicAuth(userName, UserPublicDiver.DEFAULT_PASSWORD)
                .getForEntity("/users/" + userName + "/friends", UserFriendsResource.class);

        //then
        assertThat(response.getStatusCode()).isEqualTo(OK);
        assertThat(response.getBody()).isEqualToComparingFieldByField(UserFriendsResource.NO_FRIENDS);
    }
}
