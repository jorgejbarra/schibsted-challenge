package es.schibsted.challenge;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public final class RequestSerializer {

    private final ObjectMapper jsonMapper;

    public RequestSerializer() {
        jsonMapper = new ObjectMapper();
    }

    public String toJson(final Object createUserRequestBody) throws JsonProcessingException {
        return jsonMapper.writeValueAsString(createUserRequestBody);
    }
}
