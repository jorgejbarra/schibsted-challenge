package es.schibsted.challenge.interfaces;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
class CreateUserRequestBody {
    private final String userName;
    private final String password;


}
