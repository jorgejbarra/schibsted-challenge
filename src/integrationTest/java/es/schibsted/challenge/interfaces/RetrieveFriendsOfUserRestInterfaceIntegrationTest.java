package es.schibsted.challenge.interfaces;

import es.schibsted.challenge.interfaces.errors.ValidationErrorsHandler;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RetrieveFriendsOfUserRestInterfaceIntegrationTest {

    private MockMvc mockMvc;

    @Before
    public void beforeTest() {

        mockMvc = MockMvcBuilders.standaloneSetup(new RetrieveFriendsOfUserRestInterface())
                .setControllerAdvice(new ValidationErrorsHandler())
                .build();
    }

    @Test
    //TODO review
    public void shouldReturnNoneFriendWhenUserIsUnknown() throws Exception {

        //given
        final String unknownUserName = "unknownUsername";


        //when
        mockMvc.perform(get("/users/" + unknownUserName + "/friends")
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isOk());
    }
}
