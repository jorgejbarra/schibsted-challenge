package es.schibsted.challenge.interfaces;

import es.schibsted.challenge.RequestSerializer;
import es.schibsted.challenge.domain.SocialNetworkService;
import es.schibsted.challenge.domain.UserMother;
import es.schibsted.challenge.interfaces.errors.ValidationErrorsHandler;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class CreateUserRestInterfaceIntegrationTest {

    private final UserResourceMapper mapper = new UserResourceMapper();
    private final SocialNetworkService socialNetworkService = mock(SocialNetworkService.class);
    private final RequestSerializer requestSerializer = new RequestSerializer();
    private MockMvc mockMvc;

    @Before
    public void beforeTest() {
        when(socialNetworkService.signUpNewUser(any())).thenReturn(Optional.of(UserMother.sample()));

        mockMvc = MockMvcBuilders.standaloneSetup(new CreateUserRestInterface(socialNetworkService, mapper))
                .setControllerAdvice(new ValidationErrorsHandler())
                .build();
    }

    @Test
    public void shouldReturnErrorWhenUsernameIsEmpty() throws Exception {

        //given
        final String emptyName = "";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody(emptyName, "samplePass")))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_UTF8))
                .andExpect(usernamePatternError());
    }

    @Test
    public void shouldReturnErrorWhenUsernameHaveMoreThan10Characters() throws Exception {

        //given
        final String nameWithMoreThan10Char = "12345123456";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody(nameWithMoreThan10Char, "samplePass")))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_UTF8))
                .andExpect(usernamePatternError());
    }

    @Test
    public void shouldReturnErrorWhenUsernameHaveLessThan5Characters() throws Exception {

        //given
        final String nameWithLessThan5Char = "1234";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody(nameWithLessThan5Char, "samplePass")))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_UTF8))
                .andExpect(usernamePatternError());
    }

    @Test
    public void shouldReturnErrorWhenUsernameHaveUnderscores() throws Exception {

        //given
        final String usernameWithUnderscores = "1234_1234";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody(usernameWithUnderscores, "samplePass")))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_UTF8))
                .andExpect(usernamePatternError());
    }

    @Test
    public void shouldReturnErrorWhenUsernameHaveDots() throws Exception {

        //given
        final String usernameWithUnderscores = "1234.1234";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody(usernameWithUnderscores, "rightPass")))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_UTF8))
                .andExpect(usernamePatternError());
    }

    @Test
    public void shouldReturnErrorWhenUsernameHaveComas() throws Exception {

        //given
        final String usernameWithUnderscores = "1234,1234";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody(usernameWithUnderscores, "rightPass")))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_UTF8))
                .andExpect(usernamePatternError());
    }

    @Test
    public void shouldReturnOkWhenUsernameHave10Characters() throws Exception {

        //given
        final String nameWith10Characters = "joseManuel";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody(nameWith10Characters, "samplePass")))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnOkWhenUsernameHave5Characters() throws Exception {

        //given
        final String nameWith5Characters = "david";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody(nameWith5Characters, "samplePass")))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isOk());
    }


    @Test
    public void shouldReturnErrorWhenPasswordIsEmpty() throws Exception {

        //given
        final String emptyPassword = "";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody("manuel", emptyPassword)))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_UTF8))
                .andExpect(passwordPatternError());
    }

    @Test
    public void shouldReturnErrorWhenPasswordHaveMoreThan12Characters() throws Exception {

        //given
        final String passwordWithMoreThan13Char = "1234561234567";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody("gustavo", passwordWithMoreThan13Char)))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_UTF8))
                .andExpect(passwordPatternError());
    }

    @Test
    public void shouldReturnErrorWhenPasswordHaveLessThan8Characters() throws Exception {

        //given
        final String passwordWithLessThan8Char = "1234567";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody("paquito", passwordWithLessThan8Char)))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_UTF8))
                .andExpect(passwordPatternError());
    }

    @Test
    public void shouldReturnErrorWhenPasswordHaveUnderscores() throws Exception {

        //given
        final String passWithUnderscores = "1234_1234";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody("dionisioII", passWithUnderscores)))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_UTF8))
                .andExpect(passwordPatternError());
    }

    @Test
    public void shouldReturnErrorWhenPasswordHaveDots() throws Exception {

        //given
        final String passWithUnderscores = "1234.1234";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody("dionisioII", passWithUnderscores)))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_UTF8))
                .andExpect(passwordPatternError());
    }

    @Test
    public void shouldReturnErrorWhenPasswordHaveComas() throws Exception {

        //given
        final String passWithUnderscores = "1234,1234";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody("dionisioII", passWithUnderscores)))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isBadRequest())
                .andExpect(content().contentTypeCompatibleWith(APPLICATION_JSON_UTF8))
                .andExpect(passwordPatternError());
    }

    @Test
    public void shouldReturnOkWhenPasswordHave8Characters() throws Exception {

        //given
        final String passWith8Characters = "12345678";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody("dionisioII", passWith8Characters)))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnOkWhenPasswordHave12Characters() throws Exception {

        //given
        final String passWith12Characters = "123456123456";

        //when
        mockMvc.perform(post("/users")
                .content(requestSerializer.toJson(new CreateUserRequestBody("dionisioII", passWith12Characters)))
                .contentType(APPLICATION_JSON))

                //then
                .andExpect(status().isOk());
    }


    private ResultMatcher usernamePatternError() {
        return ResultMatcher.matchAll(
                jsonPath("$.errors[0].source.pointer", is("userName")),
                jsonPath("$.errors[0].title", is("Username must have 5-10 characters, ONLY alphanumeric characters (no dots no commas no underscores)")),
                jsonPath("$.errors[0].code", is("Pattern.userName")));
    }

    private ResultMatcher passwordPatternError() {
        return ResultMatcher.matchAll(
                jsonPath("$.errors[0].source.pointer", is("password")),
                jsonPath("$.errors[0].title", is("Password must have 8-12 characters, ONLY alphanumeric (no dots no commas no underscores)")),
                jsonPath("$.errors[0].code", is("Pattern.password")));
    }

}
