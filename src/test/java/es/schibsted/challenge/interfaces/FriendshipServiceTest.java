package es.schibsted.challenge.interfaces;


import es.schibsted.challenge.domain.Friendship;
import es.schibsted.challenge.infrastructure.user.UserRepository;
import org.junit.Test;

import java.util.Optional;

import static es.schibsted.challenge.domain.UserMother.hiddenUserWithName;
import static es.schibsted.challenge.domain.UserMother.publicUserWithName;
import static java.util.Optional.empty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FriendshipServiceTest {

    private final UserRepository userRepository = mock(UserRepository.class);
    private final FriendshipService friendshipService = new FriendshipService(userRepository);

    @Test
    public void shouldReturnEmptyWhenSourceIsUnknown() {

        //given
        final String unknownUsername = "jose";
        final String knownUserName = "jorge";

        when(userRepository.findByUsername(unknownUsername)).thenReturn(empty());

        //when
        final Optional<Friendship> friendship = friendshipService.createFriendship(unknownUsername, knownUserName);

        //then
        assertThat(friendship).isEmpty();
    }

    @Test
    public void shouldReturnEmptyWhenSourceUserExistButTargetIsUnknown() {

        //given
        final String unknownUsername = "alfonso";
        final String knownUserName = "vanessa";

        when(userRepository.findByUsername(knownUserName)).thenReturn(Optional.of(publicUserWithName(knownUserName)));
        when(userRepository.findByUsername(unknownUsername)).thenReturn(empty());

        //when
        final Optional<Friendship> friendship = friendshipService.createFriendship(knownUserName, unknownUsername);

        //then
        assertThat(friendship).isEmpty();
    }

    @Test
    public void shouldReturnEmptyWhenTargetUserExistButHaveHiddenVisibility() {

        //given
        final String source = "dionisio";
        final String target = "enrique";

        when(userRepository.findByUsername(source)).thenReturn(Optional.of(publicUserWithName(source)));
        when(userRepository.findByUsername(target)).thenReturn(Optional.of(hiddenUserWithName(target)));

        //when
        final Optional<Friendship> friendship = friendshipService.createFriendship(source, target);

        //then
        assertThat(friendship).isEmpty();
    }

    @Test
    public void shouldReturnFriendshipWhenStoreFriendship() {

        //given
        final String source = "maria";
        final String target = "josefa";

        when(userRepository.findByUsername(source)).thenReturn(Optional.of(publicUserWithName(source)));
        when(userRepository.findByUsername(target)).thenReturn(Optional.of(publicUserWithName(target)));

        //when
        final Optional<Friendship> friendship = friendshipService.createFriendship(source, target);

        //then
        assertThat(friendship).get().isEqualTo(Friendship.builder()
                .sourceUsername(source)
                .targetUsername(target)
                .build());
    }

    @Test
    public void shouldReturnFriendshipWhenSourceUserHaveHiddenVisibility() {

        //given
        final String source = "lucia";
        final String target = "mario";

        when(userRepository.findByUsername(source)).thenReturn(Optional.of(hiddenUserWithName(source)));
        when(userRepository.findByUsername(target)).thenReturn(Optional.of(publicUserWithName(target)));

        //when
        final Optional<Friendship> friendship = friendshipService.createFriendship(source, target);

        //then
        assertThat(friendship).get().isEqualTo(Friendship.builder()
                .sourceUsername(source)
                .targetUsername(target)
                .build());
    }
}
