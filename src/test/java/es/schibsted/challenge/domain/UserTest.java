package es.schibsted.challenge.domain;

import org.junit.Test;

import java.time.Instant;
import java.util.Random;

import static es.schibsted.challenge.domain.ProfileVisibility.PUBLIC;
import static org.assertj.core.api.Assertions.assertThat;

public class UserTest {

    @Test
    public void shouldNotModifyUserWhenAddFriendship() {

        //given
        final User sample = User.builder()
                .id(new Random().nextLong())
                .userName("fernando")
                .password("password")
                .profileVisibility(PUBLIC)
                .signUpDate(Instant.now())
                .build();

        //when
        final User modifiedUser = sample.addFriendship("jorge");

        //then
        assertThat(modifiedUser).isNotSameAs(sample);
        assertThat(sample.getFriendships()).isEmpty();
        assertThat(modifiedUser.getFriendships()).contains(Friendship.builder()
                .sourceUsername("fernando")
                .targetUsername("jorge")
                .build());
    }

    @Test
    public void shouldAddNewFriendshipToPreviousOne() {

        //given
        final String userName = "fernando";
        final Friendship previousFriendship = Friendship.builder()
                .sourceUsername(userName)
                .targetUsername("jorge")
                .build();

        final User sample = User.builder()
                .id(new Random().nextLong())
                .userName(userName)
                .password("password")
                .profileVisibility(PUBLIC)
                .signUpDate(Instant.now())
                .friendship(previousFriendship)
                .build();

        final Friendship newFriendship = Friendship.builder()
                .sourceUsername(userName)
                .targetUsername("erik")
                .build();

        //when
        final User modifiedUser = sample.addFriendship("erik");

        //then
        assertThat(modifiedUser.getFriendships()).contains(previousFriendship, newFriendship);
    }
}
