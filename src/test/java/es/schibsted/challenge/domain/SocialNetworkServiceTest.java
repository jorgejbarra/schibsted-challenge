package es.schibsted.challenge.domain;

import es.schibsted.challenge.infrastructure.Clock;
import es.schibsted.challenge.infrastructure.user.UserRepository;
import es.schibsted.challenge.interfaces.CreateUserCommand;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.time.Instant;
import java.util.Optional;

import static es.schibsted.challenge.domain.ProfileVisibility.PUBLIC;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SocialNetworkServiceTest {


    private final Clock clockMock = mock(Clock.class);
    private final UserRepository userRepositoryMock = mock(UserRepository.class);
    private SocialNetworkService socialNetworkService;

    @Before
    public void setUp() {
        when(clockMock.getCurrentDate()).thenReturn(Instant.now());
        when(userRepositoryMock.store(any())).then(returnsFirstArg());

        socialNetworkService = new SocialNetworkService(userRepositoryMock, clockMock);
    }

    @Test
    public void shouldReturnNewUserWithIdWhenSignUpNewUser() {

        //given
        final String expectedName = "JuanManuel";
        final String expectedPassword = "experience";

        //when
        final Optional<User> createdUser = socialNetworkService.signUpNewUser(new CreateUserCommand(expectedName, expectedPassword));

        //then
        assertThat(createdUser).isPresent()
                .get().as("Id must be not null").hasFieldOrProperty("id")
                .as("Username must be correct").hasFieldOrPropertyWithValue("userName", expectedName)
                .as("Password must be correct").hasFieldOrPropertyWithValue("password", expectedPassword.getBytes());
    }

    @Test
    public void shouldThrowExceptionWhenSignUpNewUserAndUsernameAlreadyExist() {

        //given
        final String existingUsername = "Salvador";
        when(userRepositoryMock.existUserName(existingUsername)).thenReturn(true);

        //when

        final Optional<User> createdUser = socialNetworkService.signUpNewUser(new CreateUserCommand(existingUsername, "samplePass"));

        //then
        assertThat(createdUser).isEmpty();
    }

    @Test
    public void shouldSetCurrentDateAsCreationDateWhenSignUpNewUser() {

        //given
        final Instant expectedDate = Instant.parse("1997-07-16T19:20:30Z");
        when(clockMock.getCurrentDate()).thenReturn(expectedDate);


        //when
        final Optional<User> createdUser = socialNetworkService.signUpNewUser(new CreateUserCommand("JuanManuel", "experience"));

        //then
        assertThat(createdUser).get()
                .hasFieldOrPropertyWithValue("signUpDate", expectedDate);

    }

    @Test
    public void shouldSetToPublicTheProfileVisibilityWhenSignUpNewUser() {

        //when
        final Optional<User> createdUser = socialNetworkService.signUpNewUser(new CreateUserCommand("Ramon", "12345678"));

        //then
        assertThat(createdUser).get()
                .hasFieldOrPropertyWithValue("profileVisibility", PUBLIC);
    }

    @Test
    public void shouldStoreANewUser() {
        //given
        final ArgumentCaptor<User> captor = ArgumentCaptor.forClass(User.class);
        final String expectedUserName = "Ramon";
        final String expectedPassword = "12345678";

        //when
        socialNetworkService.signUpNewUser(new CreateUserCommand(expectedUserName, expectedPassword));

        //then
        verify(userRepositoryMock).store(captor.capture());
        assertThat(captor.getValue()).hasFieldOrPropertyWithValue("userName", expectedUserName);
        assertThat(captor.getValue()).hasFieldOrPropertyWithValue("password", expectedPassword.getBytes());
    }
}
