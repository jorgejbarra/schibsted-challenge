package es.schibsted.challenge.domain;


import es.schibsted.challenge.infrastructure.athorization.SecurityContextHolderFacade;
import org.junit.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuthenticationFacadeTest {

    private final SecurityContextHolderFacade securityContextHolderFacade = mock(SecurityContextHolderFacade.class);

    private final AuthenticationFacade authenticationFacade = new AuthenticationFacade(securityContextHolderFacade);

    @Test
    public void shouldReturnEmptyWhenContextIsNull() {

        //given
        when(securityContextHolderFacade.getContext()).thenReturn(null);

        //when
        final Optional<String> currentAuthenticationUsername = authenticationFacade.getCurrentAuthenticationUsername();

        //then
        assertThat(currentAuthenticationUsername).isEmpty();
    }

    @Test
    public void shouldReturnEmptyWhenAuthenticationIsNull() {

        //given
        final SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContextHolderFacade.getContext()).thenReturn(securityContext);
        when(securityContext.getAuthentication()).thenReturn(null);

        //when
        final Optional<String> currentAuthenticationUsername = authenticationFacade.getCurrentAuthenticationUsername();

        //then
        assertThat(currentAuthenticationUsername).isEmpty();
    }

    @Test
    public void shouldReturnEmptyWhenAuthenticationNameIsNull() {

        //given
        final SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContextHolderFacade.getContext()).thenReturn(securityContext);
        when(securityContext.getAuthentication()).thenReturn(authentication(null));

        //when
        final Optional<String> currentAuthenticationUsername = authenticationFacade.getCurrentAuthenticationUsername();

        //then
        assertThat(currentAuthenticationUsername).isEmpty();
    }

    @Test
    public void shouldReturnAuthenticationNameWhenAuthenticationIsNotNull() {

        //given
        final SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContextHolderFacade.getContext()).thenReturn(securityContext);
        when(securityContext.getAuthentication()).thenReturn(authentication("jorge"));

        //when
        final Optional<String> currentAuthenticationUsername = authenticationFacade.getCurrentAuthenticationUsername();

        //then
        assertThat(currentAuthenticationUsername).get().isEqualTo("jorge");
    }

    private Authentication authentication(final String username) {
        return new UsernamePasswordAuthenticationToken(username, "password");
    }
}
