package es.schibsted.challenge.domain;

import java.time.Instant;
import java.util.Random;

import static es.schibsted.challenge.domain.ProfileVisibility.HIDDEN;
import static es.schibsted.challenge.domain.ProfileVisibility.PUBLIC;

public final class UserMother {

    private UserMother() {
    }

    public static User hiddenUserWithName(final String expectedUsername) {
        return User.builder()
                .id(new Random().nextLong())
                .userName(expectedUsername)
                .password("12345678")
                .profileVisibility(HIDDEN)
                .signUpDate(Instant.now())
                .build();
    }

    public static User publicUserWithName(final String expectedUsername) {
        return User.builder()
                .id(new Random().nextLong())
                .userName(expectedUsername)
                .password("12345678")
                .profileVisibility(PUBLIC)
                .signUpDate(Instant.now())
                .build();
    }
}
