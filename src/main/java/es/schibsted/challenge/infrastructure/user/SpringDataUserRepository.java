package es.schibsted.challenge.infrastructure.user;

import es.schibsted.challenge.domain.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
@SuppressWarnings("unused")
class SpringDataUserRepository implements UserRepository {

    private final JpaUserRepository delegate;
    private final UserEntityMapper mapper;

    @Override
    public Boolean existUserName(final String userNameToCheck) {

        return delegate.findByUserName(userNameToCheck).isPresent();
    }

    @Override
    public User store(final User userToStore) {

        return mapper.map(delegate.save(mapper.map(userToStore)));
    }

    @Override
    public Optional<User> findByUsername(final String username) {
        return delegate.findByUserName(username).map(mapper::map);
    }
}
