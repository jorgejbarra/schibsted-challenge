package es.schibsted.challenge.infrastructure.user;

import es.schibsted.challenge.domain.User;

import java.util.Optional;

public interface UserRepository {
    Boolean existUserName(final String userNameToCheck);

    User store(final User userToStore);

    Optional<User> findByUsername(final String username);
}
