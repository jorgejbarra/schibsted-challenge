package es.schibsted.challenge.infrastructure.athorization;

import es.schibsted.challenge.infrastructure.user.JpaUserRepository;
import es.schibsted.challenge.infrastructure.user.UserEntity;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import static java.util.Collections.emptyList;
import static lombok.AccessLevel.PRIVATE;

@SuppressWarnings("unused")
@Component
@AllArgsConstructor(access = PRIVATE)
public class SpringDataUserDetailsService implements UserDetailsService {

    private final JpaUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        return userRepository.findByUserName(username)
                .map(this::toUserDetails)
                .orElseThrow(() -> new UsernameNotFoundException("No user found with name :" + username));
    }

    private UserDetails toUserDetails(final UserEntity source) {
        return new org.springframework.security.core.userdetails.User(source.getUserName(), new String(source.getPassword()),
                true, true, true, true,
                emptyList());
    }


}
