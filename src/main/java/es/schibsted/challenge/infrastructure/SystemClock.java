package es.schibsted.challenge.infrastructure;

import org.springframework.stereotype.Component;

import java.time.Instant;

@Component
class SystemClock implements Clock {


    @Override
    public Instant getCurrentDate() {
        return Instant.now();
    }
}
