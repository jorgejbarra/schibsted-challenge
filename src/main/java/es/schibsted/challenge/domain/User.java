package es.schibsted.challenge.domain;


import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Singular;
import lombok.ToString;

import java.time.Instant;
import java.util.Collections;
import java.util.List;

import static java.util.Collections.emptyList;
import static org.apache.commons.lang3.Validate.matchesPattern;
import static org.apache.commons.lang3.Validate.notNull;

@ToString
@EqualsAndHashCode
@Getter
@Builder(toBuilder = true)
public final class User {

    private final Long id;
    private final String userName;
    private final byte[] password;
    private final Instant signUpDate;
    private final ProfileVisibility profileVisibility;
    @Singular
    private final List<Friendship> friendships;

    private User(final UserBuilder userBuilder) {
        this.id = userBuilder.id;
        this.userName = userBuilder.userName;
        this.password = userBuilder.password.getBytes();
        this.signUpDate = userBuilder.signUpDate;
        this.profileVisibility = userBuilder.profileVisibility;

        if (userBuilder.friendships == null) {
            this.friendships = emptyList();
        } else {
            this.friendships = Collections.unmodifiableList(userBuilder.friendships);
        }
    }

    public static UserBuilder builder() {
        return new UserBuilder();
    }

    public User addFriendship(final String targetUsername) {
        return this.toBuilder().friendship(Friendship.builder()
                .sourceUsername(this.userName)
                .targetUsername(targetUsername)
                .build())
                .build();
    }

    public static class UserBuilder {

        private String password;

        public UserBuilder password(final byte[] newValue) {
            this.password = new String(newValue);
            return this;
        }

        public UserBuilder password(final String newValue) {
            this.password = newValue;
            return this;
        }

        public User build() {
            notNull(userName, "Username is required");
            notNull(password, "Password ID is required");
            notNull(signUpDate, "SignUpDate ID is required");
            notNull(profileVisibility, "ProfileVisibility ID is required");
            matchesPattern(password, "^[a-zA-Z0-9]{8,12}$", "Password must have 8-12 characters, ONLY alphanumeric (no dots no commas no underscores)");

            return new User(this);
        }
    }
}
