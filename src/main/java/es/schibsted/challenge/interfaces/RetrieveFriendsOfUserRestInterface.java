package es.schibsted.challenge.interfaces;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import static lombok.AccessLevel.PACKAGE;
import static org.springframework.http.ResponseEntity.ok;

@RestController
@AllArgsConstructor(access = PACKAGE)
class RetrieveFriendsOfUserRestInterface {


    @GetMapping("/users/{userName}/friends")
    ResponseEntity<UserFriendsResource> retrieveFriendsOfUser(@PathVariable("userName") final String userName) {
        return ok(UserFriendsResource.NO_FRIENDS);
    }
}
