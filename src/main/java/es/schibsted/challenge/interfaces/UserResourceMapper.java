package es.schibsted.challenge.interfaces;

import es.schibsted.challenge.domain.User;
import org.springframework.stereotype.Component;

@Component
class UserResourceMapper {
    UserResource map(final User source) {
        return UserResource.builder()
                .userName(source.getUserName())
                .signUpDate(source.getSignUpDate())
                .profileVisibility(source.getProfileVisibility())
                .build();
    }
}
