package es.schibsted.challenge.interfaces;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Pattern;

import static lombok.AccessLevel.PRIVATE;

@NoArgsConstructor(access = PRIVATE)
public class CreateUserCommand {

    @JsonProperty("userName")
    @Pattern(regexp = "^[a-zA-Z0-9]{5,10}$", message = "Username must have 5-10 characters, ONLY alphanumeric characters (no dots no commas no underscores)")
    private String userName;

    @JsonProperty("password")
    @Pattern(regexp = "^[a-zA-Z0-9]{8,12}$", message = "Password must have 8-12 characters, ONLY alphanumeric (no dots no commas no underscores)")
    private String password;

    public CreateUserCommand(final String userName, final String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

}
