package es.schibsted.challenge.interfaces;

import es.schibsted.challenge.domain.ProfileVisibility;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.Instant;

import static lombok.AccessLevel.PRIVATE;

@Builder
@Getter
@NoArgsConstructor(access = PRIVATE)
@AllArgsConstructor(access = PRIVATE)
class UserResource {

    private String userName;
    private Instant signUpDate;
    private ProfileVisibility profileVisibility;
}
