package es.schibsted.challenge.interfaces;

import es.schibsted.challenge.domain.Friendship;
import es.schibsted.challenge.domain.User;
import es.schibsted.challenge.infrastructure.user.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static es.schibsted.challenge.domain.ProfileVisibility.HIDDEN;
import static lombok.AccessLevel.PROTECTED;

@Service
@AllArgsConstructor(access = PROTECTED)
class FriendshipService {

    private final UserRepository userRepository;

    Optional<Friendship> createFriendship(final String sourceUsername, final String targetUsername) {

        final Optional<User> sourceUser = userRepository.findByUsername(sourceUsername);
        final Optional<User> targetUser = userRepository.findByUsername(targetUsername);

        if (!sourceUser.isPresent()) {
            return Optional.empty();
        }

        if (!targetUser.isPresent()) {
            return Optional.empty();
        }

        if (targetUser.get().getProfileVisibility() == HIDDEN) {
            return Optional.empty();
        }

        storeFriendship(sourceUser.get(), targetUser.get());

        return Optional.of(Friendship.builder()
                .sourceUsername(sourceUsername)
                .targetUsername(targetUsername)
                .build());
    }

    private void storeFriendship(final User sourceUser, final User targetUser) {

        final User modifiedUser = sourceUser.addFriendship(targetUser.getUserName());
        userRepository.store(modifiedUser);
    }
}
