package es.schibsted.challenge.interfaces.errors;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.io.Serializable;

@Getter
@Builder
@EqualsAndHashCode
class ErrorResource implements Serializable {

    @JsonProperty("source")
    @JsonSerialize(using = ErrorSourceSerializer.class)
    private String sourcePointer;
    @JsonProperty("code")
    private String code;
    @JsonProperty("title")
    private String title;

}
