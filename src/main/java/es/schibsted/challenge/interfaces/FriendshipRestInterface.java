package es.schibsted.challenge.interfaces;

import es.schibsted.challenge.domain.AuthenticationFacade;
import es.schibsted.challenge.domain.Friendship;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

import static lombok.AccessLevel.PROTECTED;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

@RestController
@AllArgsConstructor(access = PROTECTED)
class FriendshipRestInterface {

    private final FriendshipService friendshipService;
    private final AuthenticationFacade authenticationFacade;

    @PostMapping("/users/{sourceUsername}/friendships/{targetUsername}")
    ResponseEntity<Friendship> createFriendship(@PathVariable("sourceUsername") final String sourceUsername,
                                                @PathVariable("targetUsername") final String targetUsername) {

        final Optional<String> currentUsername = authenticationFacade.getCurrentAuthenticationUsername();
        if (!currentUsername.isPresent()) {
            return responseWithStatus(UNAUTHORIZED);
        }

        if (sourceUserIsNotAuthorized(currentUsername.get(), sourceUsername)) {
            return responseWithStatus(FORBIDDEN);
        }


        return friendshipService.createFriendship(sourceUsername, targetUsername)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    private boolean sourceUserIsNotAuthorized(final String principal, final String sourceUsername) {
        return !principal.equals(sourceUsername);
    }

    private ResponseEntity<Friendship> responseWithStatus(final HttpStatus forbidden) {
        return ResponseEntity.status(forbidden).build();
    }
}
